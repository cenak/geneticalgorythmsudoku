﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace SudokuWF
{
    public partial class MainForm : Form
    {
        static Task task = new Task();
        Solution solution = new Solution(task);
        ExpectantSolution[] expectantSolutions = new ExpectantSolution[100];
        Descendant descendant = new Descendant(task);

        Label[] fitnessFunctionsOfExpectantSolutions = new Label[100];

        bool pause = false;
        long iterations = 0;

        public MainForm()
        {
            InitializeComponent();
            /*
            for (int i = 0; i < expectantSolutions.Length; i++)
            {
                fitnessFunctionsOfExpectantSolutions[i] = new Label();
                fitnessFunctionsOfExpectantSolutions[i].Text = "1337";
                //fitnessFunctionsOfExpectantSolutions[i].Anchor anchor 
                //ContainerOfPersons.Controls.Add(fitnessFunctionsOfExpectantSolutions[i]);
            }*/

            for (int i = 0; i < expectantSolutions.Length; i++)
                expectantSolutions[i] = new ExpectantSolution(task);

        }

        public void CalculatingTheSolving()
        {
            iterations = 0;
            Random rnd = new Random();
            Random mutationRnd = new Random();
            Random rndP1 = new Random();
            Random rndP2 = new Random();

            StaticAndNotStaticConnector connector = new StaticAndNotStaticConnector();

            int parent1 = 0;
            int parent2 = 0;

            for (int i = 0; i < expectantSolutions.Length; i++)
                expectantSolutions[i] = new ExpectantSolution(task);

            for (int i = 0; i < expectantSolutions.Length; i++)
                for (int j = 0; j < 81; j++)
                    if(task.GetElementInField((byte)j) == 0)
                        expectantSolutions[i].SetElementInField((byte)(rnd.Next(9) + 1), (byte)j);

            do
            {
                //Check for expectant solution with zero fitness function
                for (int i = 0; i < expectantSolutions.Length; i++)
                {
                    expectantSolutions[i].CalculateFitnessFunction();

                    if (expectantSolutions[i].GetFitnessFunction() == 0)
                    {
                        solution.SetField(expectantSolutions[i].GetField());
                        solution.CalculateFitnessFunction();
                    }
                }

                if (solution.GetFitnessFunction() == 0)
                    break;

                //Crossbreeding
                parent1 = rndP1.Next(expectantSolutions.Length);
                do {
                    parent2 = rndP2.Next(expectantSolutions.Length);
                } while (parent2 == parent1);

                descendant.NewDescendant(expectantSolutions[parent1], expectantSolutions[parent2], task);

                if (expectantSolutions[parent1].GetFitnessFunction() < expectantSolutions[parent2].GetFitnessFunction() &&
                   descendant.GetFitnessFunction() < expectantSolutions[parent2].GetFitnessFunction())
                    expectantSolutions[parent2].SetField(descendant.GetField());

                else if (expectantSolutions[parent2].GetFitnessFunction() < expectantSolutions[parent1].GetFitnessFunction() &&
                         descendant.GetFitnessFunction() < expectantSolutions[parent1].GetFitnessFunction())
                    expectantSolutions[parent1].SetField(descendant.GetField());

                //Mutation
                if (mutationRnd.Next(100) < 4)
                    expectantSolutions[rnd.Next(expectantSolutions.Length)].Mutation(task);

                iterations++;

                SetText("Iterations: " + iterations, descendant.GetFitnessFunction().ToString());

                if (pause)
                    break;

            } while (solution.GetFitnessFunction() != 0);

            MessageBox.Show(String.Format("Solution was found for {0} iterations!", iterations));

            pause = false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnContinue.Enabled = false;
            btnPause.Enabled = true;
            btnStart.Text = "Restart";

            if (backgroundWorker1.IsBusy != true)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        delegate void StringArgReturningVoidDelegate(string textIterations, string textDescedantsFitnessFunction);

        private void SetText(string textIterations, string textDescedantsFitnessFunction)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (lblIterations.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(SetText);
                Invoke(d, new object[] { textIterations, textDescedantsFitnessFunction });
            }
            else
            {
                lblIterations.Text = textIterations;
                lblDescendantFitnessFunction.Text = textDescedantsFitnessFunction;
            }
        }


        private void btnPause_Click(object sender, EventArgs e)
        {
            btnContinue.Enabled = true;
            btnPause.Enabled = false;

            //pause = true;

            if (backgroundWorker1.IsBusy == true)
            {
                backgroundWorker1.CancelAsync();
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            btnContinue.Enabled = false;
            btnPause.Enabled = true;

            do
            {
                if (pause)
                    break;

            } while (solution.GetFitnessFunction() != 0);

            pause = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            CalculatingTheSolving();
        }
    }
}
