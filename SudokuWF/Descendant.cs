﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuWF
{
    class Descendant : Solution
    {

        public Descendant()
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = 0;

            fitnessFunction = 1;
        }

        public Descendant(Task task)
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = task.GetElementInField((byte)i);

            fitnessFunction = 1;
        }

        public void NewDescendant(ExpectantSolution p1, ExpectantSolution p2, Task task)
        {
            Random rnd = new Random();

            for (int i = 0; i < 81; i++)
            {
                if (task.GetElementInField((byte)i) == 0)
                {
                    if (rnd.Next(2) == 0)
                        SetElementInField(p1.GetElementInField((byte)i), (byte)i);
                    else
                        SetElementInField(p2.GetElementInField((byte)i), (byte)i);
                }
            }

            CalculateFitnessFunction();
        }
    }
}
