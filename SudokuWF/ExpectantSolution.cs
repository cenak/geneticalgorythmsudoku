﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuWF
{
    class ExpectantSolution : Solution
    {
        public ExpectantSolution()
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = 0;

            fitnessFunction = 1;
        }

        public ExpectantSolution(Task task)
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = task.GetElementInField((byte)i);

            fitnessFunction = 1;
        }

        public void Mutation(Task task)
        {
            Random randNumber = new Random();
            Random randPosition = new Random();

            byte number = Convert.ToByte(1 + randNumber.Next(9));
            byte position = Convert.ToByte(randPosition.Next(81));

            if (task.GetElementInField(position) == 0)
            {
                SetElementInField(number, position);
            }
        }

        /*public static ExpectantSolution operator +(ExpectantSolution es)
        {
            ExpectantSolution exS = new

            return new ExpectantSolution { fe };
        }*/
    }
}
