﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuWF
{
    class Solution
    {
        protected byte[] field = new byte[81];
        protected int fitnessFunction;

        /*public void Initialisation()
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = 0;

            fitnessFunction = 1; ;
        }*/

        public Solution()
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = 0;

            fitnessFunction = 1;
        }

        public Solution(Task task)
        {
            for (int i = 0; i < field.Length; i++)
                field[i] = task.GetElementInField((byte)i);

            fitnessFunction = 1;
        }

        public byte[] GetField()
        {
            return field;
        }

        public byte GetElementInField(byte position)
        {
            return field[position];
        }

        public void SetElementInField(byte number, byte position)
        {
            field[position] = number;
        }

        public void SetField(byte[] _field)
        {
            for(int i = 0; i < field.Length; i++)
                field[i] = _field[i];
        }

        public int GetFitnessFunction()
        {
            return fitnessFunction;
        }

        //Checked, good
        public void CalculateFitnessFunction()
        {
            fitnessFunction = 0;

            byte[] tmpArray = new byte[9];

            //Проход по строкам
            for (int i = 0; i < 80; i += 9)
            {
                for (int j = 0; j < 9; j++)
                    tmpArray[j] = field[i + j];

                Array.Sort(tmpArray);

                for (int j = 0; j < 9; j++)
                    fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);
            }

            //Проход по столбцам
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < i + 73; j += 9)
                    tmpArray[i] = field[i + j];

                Array.Sort(tmpArray);

                for (int j = 0; j < 9; j++)
                    fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);
            }

            byte[] tmp = new byte[] {0, 1, 2, 9, 10, 11, 18, 19, 20};
            //Проход по квадратам

            //1
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i]];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //2
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 3];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //3
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 6];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //4
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 27];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //5
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 30];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //6
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 33];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //7
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 54];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //8
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 57];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);

            //9
            for (int i = 0; i < 9; i++)
                tmpArray[i] = field[tmp[i] + 60];

            Array.Sort(tmpArray);

            for (int j = 0; j < 9; j++)
                fitnessFunction += (int)Math.Pow(tmpArray[j] - (j + 1), 2);
        }
    }
}
