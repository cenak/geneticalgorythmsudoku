﻿namespace SudokuWF
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContainerOfPersons = new System.Windows.Forms.GroupBox();
            this.ContainerOfDescendant = new System.Windows.Forms.GroupBox();
            this.lblDescendantFitnessFunction = new System.Windows.Forms.Label();
            this.ContainerOfSolution = new System.Windows.Forms.GroupBox();
            this.lblIterations = new System.Windows.Forms.Label();
            this.btnOneStep = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnShowSudoku = new System.Windows.Forms.Button();
            this.btnSetSudoku = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblSolutionsFitnessFunction = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ContainerOfPersons
            // 
            this.ContainerOfPersons.Location = new System.Drawing.Point(13, 13);
            this.ContainerOfPersons.Name = "ContainerOfPersons";
            this.ContainerOfPersons.Size = new System.Drawing.Size(480, 402);
            this.ContainerOfPersons.TabIndex = 0;
            this.ContainerOfPersons.TabStop = false;
            this.ContainerOfPersons.Text = "Persons (100)";
            // 
            // ContainerOfDescendant
            // 
            this.ContainerOfDescendant.Location = new System.Drawing.Point(517, 13);
            this.ContainerOfDescendant.Name = "ContainerOfDescendant";
            this.ContainerOfDescendant.Size = new System.Drawing.Size(86, 402);
            this.ContainerOfDescendant.TabIndex = 1;
            this.ContainerOfDescendant.TabStop = false;
            this.ContainerOfDescendant.Text = "Descendant";
            // 
            // lblDescendantFitnessFunction
            // 
            this.lblDescendantFitnessFunction.AutoSize = true;
            this.lblDescendantFitnessFunction.Location = new System.Drawing.Point(517, 399);
            this.lblDescendantFitnessFunction.Name = "lblDescendantFitnessFunction";
            this.lblDescendantFitnessFunction.Size = new System.Drawing.Size(87, 13);
            this.lblDescendantFitnessFunction.TabIndex = 0;
            this.lblDescendantFitnessFunction.Text = "Descendant\'s FF";
            this.lblDescendantFitnessFunction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ContainerOfSolution
            // 
            this.ContainerOfSolution.Location = new System.Drawing.Point(626, 13);
            this.ContainerOfSolution.Name = "ContainerOfSolution";
            this.ContainerOfSolution.Size = new System.Drawing.Size(59, 402);
            this.ContainerOfSolution.TabIndex = 2;
            this.ContainerOfSolution.TabStop = false;
            this.ContainerOfSolution.Text = "Solution";
            // 
            // lblIterations
            // 
            this.lblIterations.AutoSize = true;
            this.lblIterations.Location = new System.Drawing.Point(13, 425);
            this.lblIterations.Name = "lblIterations";
            this.lblIterations.Size = new System.Drawing.Size(62, 13);
            this.lblIterations.TabIndex = 3;
            this.lblIterations.Text = "Iterations: 0";
            // 
            // btnOneStep
            // 
            this.btnOneStep.Location = new System.Drawing.Point(367, 420);
            this.btnOneStep.Name = "btnOneStep";
            this.btnOneStep.Size = new System.Drawing.Size(75, 23);
            this.btnOneStep.TabIndex = 4;
            this.btnOneStep.Text = "One Step";
            this.btnOneStep.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(448, 420);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.Location = new System.Drawing.Point(610, 420);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 6;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnShowSudoku
            // 
            this.btnShowSudoku.Location = new System.Drawing.Point(244, 420);
            this.btnShowSudoku.Name = "btnShowSudoku";
            this.btnShowSudoku.Size = new System.Drawing.Size(95, 23);
            this.btnShowSudoku.TabIndex = 7;
            this.btnShowSudoku.Text = "Show Sudoku";
            this.btnShowSudoku.UseVisualStyleBackColor = true;
            // 
            // btnSetSudoku
            // 
            this.btnSetSudoku.Location = new System.Drawing.Point(143, 420);
            this.btnSetSudoku.Name = "btnSetSudoku";
            this.btnSetSudoku.Size = new System.Drawing.Size(95, 23);
            this.btnSetSudoku.TabIndex = 8;
            this.btnSetSudoku.Text = "Set Sudoku";
            this.btnSetSudoku.UseVisualStyleBackColor = true;
            // 
            // btnContinue
            // 
            this.btnContinue.Enabled = false;
            this.btnContinue.Location = new System.Drawing.Point(529, 420);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(75, 23);
            this.btnContinue.TabIndex = 9;
            this.btnContinue.Text = "Continue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // lblSolutionsFitnessFunction
            // 
            this.lblSolutionsFitnessFunction.AutoSize = true;
            this.lblSolutionsFitnessFunction.Location = new System.Drawing.Point(623, 399);
            this.lblSolutionsFitnessFunction.Name = "lblSolutionsFitnessFunction";
            this.lblSolutionsFitnessFunction.Size = new System.Drawing.Size(67, 13);
            this.lblSolutionsFitnessFunction.TabIndex = 0;
            this.lblSolutionsFitnessFunction.Text = "Solution\'s FF";
            this.lblSolutionsFitnessFunction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 450);
            this.Controls.Add(this.lblSolutionsFitnessFunction);
            this.Controls.Add(this.lblDescendantFitnessFunction);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.btnSetSudoku);
            this.Controls.Add(this.btnShowSudoku);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnOneStep);
            this.Controls.Add(this.lblIterations);
            this.Controls.Add(this.ContainerOfSolution);
            this.Controls.Add(this.ContainerOfDescendant);
            this.Controls.Add(this.ContainerOfPersons);
            this.Name = "MainForm";
            this.Text = "Genetic algorithm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ContainerOfPersons;
        private System.Windows.Forms.GroupBox ContainerOfDescendant;
        private System.Windows.Forms.GroupBox ContainerOfSolution;
        private System.Windows.Forms.Button btnOneStep;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnShowSudoku;
        private System.Windows.Forms.Button btnSetSudoku;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Label lblIterations;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lblDescendantFitnessFunction;
        private System.Windows.Forms.Label lblSolutionsFitnessFunction;
    }
}

